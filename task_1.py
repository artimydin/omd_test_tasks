import typing

DEFAULT_DELIMITER = ' '


class Item:
    def __init__(self, value: typing.Any, next_=None):
        self._value = value
        self.next = next_

class List:
    def __init__(self, *args, is_test_case: bool =False):
        self._head: typing.Optional[Item] = None
        self._tail: typing.Optional[Item] = None
        self._iter_item: typing.Optional[Item] = None
        self._is_test_case: bool = is_test_case
        if not args:
            return

        for value in args:
            self.append(value)

    @property
    def _value(self):
        return self._head._value
    
    @_value.setter
    def _value(self, value: typing.Any):
        self._head._value = value

    def __iter__(self):
        self._iter_item = self._head
        return self

    def __next__(self) -> int:
        if self._iter_item is not None:
            res = self._iter_item._value
            self._iter_item = self._iter_item.next
            return res
        raise StopIteration

    def append(self, value: typing.Any):
        item = Item(value)
        if self._head is None:
            self._head = item
        else:
            self._tail.next = item
        self._tail = item

    def __iadd__(self, iter_obj: typing.Iterable[typing.Any]):
        for obj in iter_obj:
            self.append(obj)
        return self

    def print(self, delimiter=DEFAULT_DELIMITER):
        res = delimiter.join(str(value) for value in self)
        if self._is_test_case:
            return res
        print(res)

    def print_reversed(self, delimiter=DEFAULT_DELIMITER):
        res = delimiter.join(reversed([str(value) for value in self]))
        if self._is_test_case:
            return res
        print(res)

