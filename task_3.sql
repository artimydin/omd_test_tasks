
WITH min_post_stats as (
	SELECT post_id, views, likes, shares, min(dttm) as min_dttm
	FROM posts_stats
	GROUP BY post_id, views, likes, shares
), max_min_post_dttm as (
	SELECT post_id, max(min_dttm) as max_min_dttm
	FROM min_post_stats
	GROUP BY post_id
), right_left_dttm as (
	SELECT 
		A.post_id as post_id,
		A.views as views,
		A.likes as likes,
		A.shares as shares,
		A.min_dttm as left_dttm,
		CASE
			WHEN A.min_dttm in (
				SELECT max_min_dttm FROM max_min_post_dttm
				WHERE A.post_id = max_min_post_dttm.post_id
			) THEN TIMESTAMP '9999-12-31 23:59:59.000000'
			ELSE B.min_dttm END as right_dttm
	FROM min_post_stats as A INNER JOIN min_post_stats as B ON A.post_id = B.post_id
	WHERE A.min_dttm < B.min_dttm or (
		A.min_dttm = B.min_dttm and
		A.min_dttm in (
			SELECT max_min_dttm
			FROM max_min_post_dttm
			WHERE A.post_id = max_min_post_dttm.post_id
		)
	)
)
SELECT
	post_id,
	views,
	likes,
	shares,
	left_dttm as effective_from,
	left_dttm + min(right_dttm - left_dttm) as effective_to
FROM right_left_dttm
GROUP BY post_id, views, likes, shares, left_dttm
ORDER BY post_id, left_dttm
