WITH min_dttm as (
    SELECT post_id, views, likes, shares, min(dttm) as min_dttm
    FROM posts_stats
    GROUP BY post_id, views, likes, shares
    ORDER BY post_id, min_dttm
)
SELECT
    post_id,
    views,
    likes,
    shares,
    min_dttm as effective_from,
    CASE
        WHEN LEAD(post_id) OVER (order by post_id, min_dttm) = post_id
            THEN LEAD(min_dttm) OVER (ORDER BY post_id, min_dttm)
        ELSE TIMESTAMP '9999-12-31 23:59:59.00000'
    END as effectivef_to
FROM min_dttm
