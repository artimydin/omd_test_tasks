import unittest

from task_1 import List

class TestList(unittest.TestCase):
    
    def test_from_task(self):
        list_ = List(1, 2, 3, is_test_case=True)
        self.assertEqual(list_.print(), '1 2 3')

        list_.append(4)
        self.assertEqual(list_.print(), '1 2 3 4')

        tail = List(5, 6, is_test_case=True)
        list_ += tail
        self.assertEqual(list_.print(), '1 2 3 4 5 6')

        tail._value = 0
        self.assertEqual(tail.print(), '0 6')
        self.assertEqual(list_.print(), '1 2 3 4 5 6')

        list_ += [7, 8]
        self.assertEqual(list_.print(), '1 2 3 4 5 6 7 8')

        list_ += ()
        self.assertEqual(list_.print(), '1 2 3 4 5 6 7 8')

        self.assertEqual(
            ' '.join(str(2 ** elem) for elem in list_),
            '2 4 8 16 32 64 128 256'
        )

        self.assertEqual(list_.print_reversed(), '8 7 6 5 4 3 2 1')

        empty_list = List(is_test_case=True)
        self.assertEqual(empty_list.print(), '')

        list_with_single_none_element = List(None, is_test_case=True)
        self.assertEqual(list_with_single_none_element.print(), 'None')

if __name__ == '__main__':
    unittest.main()
